package ru.t1.dzelenin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.repository.dto.IDTORepository;
import ru.t1.dzelenin.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractDTORepository<M extends AbstractModelDTO> implements IDTORepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void set(@NotNull final Collection<M> models) {
        clear();
        models.forEach(this::add);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}

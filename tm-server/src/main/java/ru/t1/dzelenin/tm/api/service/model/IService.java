package ru.t1.dzelenin.tm.api.service.model;

import ru.t1.dzelenin.tm.api.repository.model.IRepository;
import ru.t1.dzelenin.tm.dto.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}
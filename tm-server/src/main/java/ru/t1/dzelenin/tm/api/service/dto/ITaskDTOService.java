package ru.t1.dzelenin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTOService extends IUserOwnedDTOService<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}